# JsonToCsv Java

Application qui converti un fichier json en un fichier csv. Elle prend en entré le fichier json à convertir et créer le fichier csv associé.

Limite du projet : L'application fonctionne seulement sur les fichiers contenant des objects json simple ou les fichier contenant une liste de fichier json simple

## Installation & get started

You need : Java, Maven, Eclipse/Intellij

Java : Download & install -> https://www.oracle.com/fr/java/

Maven :

	- Download -> https://maven.apache.org/download.cgi

	- Install -> https://maven.apache.org/install.html

	- How to use it -> https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html

Eclipse : https://www.eclipse.org/downloads/

Intellij : https://www.jetbrains.com/idea/download/

### A venir
	
	- Pouvoir utiliser le fichier de configuration pour générer de nouveaux attributs

	- Convertir un fichier json contenant des objets

	- Convertir un fichier csv en json

## Déploiement

	- Recupérer le projet avec git clone ou autre
	
	- Mettre dans "src/main/resources" le fichier json à convertir. De base un fichier testjson.json se trouve dans le dossier.
	
	- Lancer la commande : mvn package
	
	- Lancer la commande java -jar JsonToCsv-0.0.1-SNAPSHOT-jar-with-dependencies.jar /monfichier.json
	
	- Le résultat se trouve à : "src/main/resources/res.csv"

## Auteurs

	Philippe Gnansounou : https://github.com/philippe131
	
	Andritsalama Ramaroson 

## ps
	
	Grand merci à : https://www.baeldung.com/java-converting-json-to-csv
