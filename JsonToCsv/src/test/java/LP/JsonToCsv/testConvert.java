package LP.JsonToCsv;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

public class testConvert {


	/**
	 * Pour tester, mettre testUni.json dans src/main/resources
	 * 
	 * @author philippegnansounou
	 * 
	 * @throws IOException : Erreur ouverture du fichier
	 *  
	 * @throws JsonProcessingException : Erreur durant traitement
	 */
	
	@Test()
	public void testConvertion() throws JsonProcessingException, IOException {
			
			InputStream link = ObjectReader.class
					.getResourceAsStream("testUni.json");
			ObjectMapper objectMapper = new ObjectMapper();

			JsonNode jsonTree = new ObjectMapper()
					.readTree(new File(
							"src/main/resources/testUni.json"));

			Convert convert = new Convert(
					jsonTree, objectMapper, link);

			assertTrue(convert.convertion());

	}

}
