package LP.JsonToCsv;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;

/**
 * 
 */

public class testApp{

	/**
	 * Pour tester, mettre testUni.json dans src/main/resources
	 * 
	 * @author andri
	 * 
	 * @throws IOException : Erreur ouverture du fichier
	 *  
	 * @throws JsonProcessingException : 
	 */
	
	@Test(expected = IOException.class)
	public void testMain() throws JsonProcessingException, IOException {
		InputStream link = ObjectReader.class
				.getResourceAsStream("/testjsozn.json");
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonTree = new ObjectMapper()
				.readTree(new File(
						"src/main/resources/testjsozn.json"));
	}
}