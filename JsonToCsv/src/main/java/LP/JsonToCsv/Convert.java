package LP.JsonToCsv;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvSchema.Builder;

/**
 * Classe utilise pour convertir un fichier json en un fichier csv
 * 
 * @author philippegnansounou
 * 
 * @version 1.0
 */
public class Convert {

	JsonNode jsonTree;
	JsonNode jsonTreeConfig;
	ObjectMapper objectMapper;
	InputStream link;

	/**
	 * 
	 * @param jsonTree : Represente le fichier json
	 * 
	 * @param objectMapper : Objet permettant de manipuler jsonTree
	 * 
	 * @param link : FileInputStream du fichier json
	 */
	public Convert(JsonNode jsonTree, 
			ObjectMapper objectMapper, InputStream link) {
		this.jsonTree = jsonTree;
		this.objectMapper = objectMapper;
		this.link = link;
	}

	/**
	 * Methode qui convertie un fichier json en fichier csv.
	 * 
	 * Elle creer un fichie de configuration vide si il n'en existe pas
	 *  
	 * @author philippegnansounou
	 * 
	 * @throws JsonGenerationException : catch in the App.main
	 * 
	 * @throws JsonMappingException : catch in the App.main
	 * 
	 * @throws IOException : catch in the App.main
	 * 
	 * @return True si le fichier convertie a bien ete cree, false sinon
	 */
	public boolean convertion() throws 
	JsonGenerationException, JsonMappingException, IOException {

		Builder csvSchemaBuilder = CsvSchema.builder();
		Car carvide = new Car();
		File config = new File("src/main/resources/config.json");

		// create the config if not exist
		if (!config.exists()) {
			objectMapper.writeValue(new 
					File("src/main/resources/config.json"),
					carvide);
		}

		// Case array
		if (jsonTree.isArray()) {
			jsonTree.get(0).fieldNames().forEachRemaining(
					fieldName -> {
						csvSchemaBuilder.addColumn(fieldName); });
		}

		// Simple json, create the config if not exist
		else {
			jsonTree.fieldNames().forEachRemaining(
					fieldName -> { 
						csvSchemaBuilder.addColumn(fieldName); });
		}

		// Write the CSV file
		CsvSchema csvSchema = csvSchemaBuilder.build().withHeader();
		CsvMapper csvMapper = new CsvMapper();
		csvMapper.writerFor(JsonNode.class)
		.with(csvSchema).writeValue(
				new File("src/main/resources/res.csv"),
				jsonTree);

		File csv = new File("src/main/resources/res.csv");

		return (csv.exists());
	}
}
