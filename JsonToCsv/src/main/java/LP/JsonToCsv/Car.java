/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LP.JsonToCsv;

/**
 * Cette classe représente une voiture 
 * de la même manière que notre fichier json
 * @author andri
 */
public class Car {
    private String brand = null;
	private int doors = 0;
	private String color = null;
	private int price = 0;
	private int kilometrage = 0;

	public Car() {
		brand = "brand";
		color = "color";
	}

	public String getBrand() { return this.brand; }

	public void setBrand(String brand) { this.brand = brand; }

	public int getDoors() { return this.doors; }

	public void setDoors(int doors) { this.doors = doors; }

	public String getColor() { return color; }

	public void setColor(String color) { this.color = color; }

	public int getPrice() { return price; }

	public void setPrice(int price) { this.price = price; }

	public int getKilometrage() { return kilometrage; }

	public void setKilometrage(int kilometrage) { this.kilometrage = kilometrage; }

}
