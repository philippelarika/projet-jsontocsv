package LP.JsonToCsv;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.File;

/**
 * Class contenant le main de l application, recupere
 * le fichier json, creer un ObjectMapper et un objet JsonNode
 * Fait appel a la methode convertion de la classe convert
 * 
 * @author andri
 */

public class App {
	
	public static void main(String[] args) {
            try {
		// Get the json file
		InputStream link = ObjectReader.class
                    .getResourceAsStream(args[0]);
		if (link == null) {
                    throw new NullPointerException
                    ("Cannot find resource file " + args[0]);
		}
		// Recover the json & the config in a JsonNode
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonTree = new ObjectMapper()
                    .readTree(new File(
			"src/main/resources" + args[0]));
		
		Convert convert = new Convert(
			jsonTree, objectMapper, link);
		boolean res = convert.convertion();
			
		if (res) { 
                    System.out.println("Le fichier convertie se trouve "
                    + "dans src/main/resources/res.csv"); }
		
		else { System.out.println("Le fichier convertie n'a pas pu être cree"); }
		
            } catch (NoSuchElementException e) {
		System.out.println(
			"NoSuchElementException");
            } catch (IOException e) {
		System.out.println("IOException");
            } catch (ArrayIndexOutOfBoundsException e) {
            	System.out.println("ArrayIndexOutOfBoundsException");
            } catch (NullPointerException e) {
		System.out.println("Null pointeur exception");
            }

	}
}